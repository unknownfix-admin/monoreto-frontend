import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    blurWindow: false,
    likesCounter: 0,
    investedCounter: 0,
    donatedCounter: 0,
    likeFilter: {
      feed: false,
      topContent: false,
      investment: false,
      charity: false
    },
    scroll: {
      position: null,
      isDownSwipe: false,
    },
    isPopupOpened: false,
    likeNotification: null,
    balance: 0,
    balanceUsd: 0,
    jwtToken: null,
    userInfo: {},
    userIsGuest: true,
    isBalanceEmptyVisible: false,
    routeHistory: [],
    isRouteToBack: false,
    stories: {
      active: [],
      activeId: -1,
      poped: []
    },
    isTopBlockVisible: true,
    usedBackArrow: false,
    cache: {
      feed: {
        items: [],
        page: 1
      },
      top: {
        items: [],
        page: 1
      },
      search: {
        recommendedUsers: [],
        foundUsers: [],
        searchCriteria: ''
      },
      personal: {
        posts: {
          feeds: [],
          charity: [],
          investments: []
        },
        page: 1
      }
    }
  },
  getters: {
    isBlankRouteHistory: state => {
      return state.routeHistory.length === 0
    },
    pathBackRouteHistory (state) {
      console.log(state.routeHistory)
      return state.routeHistory[0]
    },
    scrollPosition (state) {
      return state.scroll.position
    }
  },
  actions: {
    logOut({ store, dispatch, commit }){
      commit('setJwtToken', null)
      commit('setUserInfo', {})
      localStorage.removeItem('monoretoJwt')
    }
  },
  mutations: {
    extractRouteHistoryPath (state) {
      state.isRouteToBack = true
      state.routeHistory.shift()
    },
    scrollContent (state, e) {
      let prevPosition = state.scroll.position
      state.scroll.position = e.target.scrollTop
      state.scroll.isDownSwipe = prevPosition < e.target.scrollTop
    },
    updateCache (state, payload) {
      state.cache[payload.type] = payload
    },
    setRouteHistory (state, from) {
      if (from.meta && from.meta.disableBackAction === true) {
        return
      }
      state.routeHistory = [from.path].concat(state.routeHistory).slice(0, 10)
    },
    setUsedBackArrow (state, flag) {
      state.usedBackArrow = flag
    },
    setLikesCounter (state, number) {
      state.likesCounter = number
    },
    setInvestedCounter (state, number) {
      state.investedCounter = number
    },
    setDonatedCounter (state, number) {
      state.donatedCounter = number
    },
    toggleLikeFilter (state, screen) {
      state.likeFilter[screen] = !state.likeFilter[screen]
    },
    disableLikeFilter (state, screen) {
      state.likeFilter[screen] = false
    },
    // Deprecated
    togglePopup (state) {
      state.isPopupOpened = !state.isPopupOpened
    },
    hidePopup (state) {
      const classList = document.body.className.trim().split(' ').filter(x => {
        return x !== '' && x !== 'overflow-hidden'
      })
      document.body.className = classList.join(' ')
      state.isPopupOpened = false
    },
    showPopup (state) {
      document.body.className = document.body.className + ' overflow-hidden'
      state.isPopupOpened = true
    },
    setLikeNotification (state, item) {
      state.likeNotification = item
      if ('__autoremove' in item && item.__autoremove) {
        setTimeout(() => {
          state.likeNotification = null
        }, 7000)
      }
    },
    setJwtToken (state, token) {
      state.jwtToken = token
    },
    setIsGuest (state, guest) {
      state.userIsGuest = Boolean(guest)
    },
    setUserInfo (state, info) {
      store.commit('setIsGuest', Object.keys(info).length === 0) // guest if info is blank
      state.userInfo = info
      state.likesCounter = info.feed_likes_counter
      state.investedCounter = info.investment_likes_counter
      state.donatedCounter = info.charity_likes_counter
      state.balance = info.fund_count
      state.balanceUsd = info.fund_count * 0.05
    },
    showIsBalanceEmpty (state) {
      if (!state.isBalanceEmptyVisible) {
        state.isBalanceEmptyVisible = true
        setTimeout(() => {
          state.isBalanceEmptyVisible = false
        }, 5000)
      }
    },
    storiesSetActiveId (state, id) {
      state.stories.activeId = id
    },
    storiesSetActive (state, stories = []) {
      state.stories.active = stories
      state.stories.poped = []
    },
    storiesPop (state) {
      if (state.stories.active.length > 0) {
        const poped = state.stories.active.pop()
        state.stories.poped.unshift(poped)
      }
    },
    storiesPush (state) {
      if (state.stories.poped.length > 0) {
        const shifted = state.stories.poped.shift()
        state.stories.active.push(shifted)
      }
    },
    setIsTopBlockVisible (state, isVisible) {
      console.log('setting', isVisible)
      state.isTopBlockVisible = isVisible
    }
  }
})

export default store
