import axios from 'axios'

const apiBaseUrl = 'http://beta.monoreto.io:8000/api/client/v1'

const apiClient = axios.create({
  baseURL: apiBaseUrl,
  timeout: 30000,
  transformRequest: [function (data, headers) {
    const token = localStorage.getItem('monoretoJwt')
    headers.common['Authorization'] = 'Bearer ' + token
    return data
  }],
})

const getErrorMessage = (error) => {
  if (error.response) {
    try {
      console.log('ERROR', error.response)
      let data = error.response.data
      if (data.error.message) {
        return data.error.message
      }
      // extract first error from array object
      if (data.error.messages && data.error.messages.length) {
        let firstMessages = data.error.messages[0]
        let messageKey = Object.keys(firstMessages)[0]
        return firstMessages[messageKey][0]
      }

      return data.error
    } catch (e) {
      return 'Caught: ' + e.message
    }
  } else if (error.request) {
    console.log(error.request)
    return 'Something went wrong. Status: ' + error.request.statusText + '. Response: ' + error.request.responseText
  } else {
    return error.message
  }
}

const redirectUrl = '/'

export { apiClient, getErrorMessage, redirectUrl, apiBaseUrl }

export default axios.create({
  // baseURL: 'http://monoreto-backend.local/api/',
  baseURL: 'http://netherenth.xyz:8012/index.php/api/',
  timeout: 5000,
  headers: {}
})
