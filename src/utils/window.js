
const isScrolledToBottom = () => {
  const scrollY = window.scrollY
  const visible = document.documentElement.clientHeight
  const pageHeight = document.documentElement.scrollHeight
  const bottomOfPage = visible + scrollY >= pageHeight - 300
  return bottomOfPage || pageHeight < visible
}

const getScrollDirection = () => {
  let lastScrollTop = 0
  return () => {
    let direction = ''
    const st = window.pageYOffset || document.documentElement.scrollTop // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
    if (st > lastScrollTop) {
      direction = 'down'
    } else {
      direction = 'up'
    }
    lastScrollTop = st <= 0 ? 0 : st // For Mobile or negative scrolling
    return direction
  }
}

export { isScrolledToBottom, getScrollDirection }
