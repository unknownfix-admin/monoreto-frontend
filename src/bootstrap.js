import { apiClient } from './utils/httpClient'
import router from './router'

/*
  @TODO remove legacy code
*/

function initializeCustomer(store) {
    const jwtToken = localStorage.getItem('monoretoJwt')
    store.commit('setJwtToken', jwtToken)
    if (jwtToken) {
      store.commit('setIsGuest', false) // if token exist
      return apiClient.get('/my/profile')
        .then(res => {
          if (res.status === 200) {
            store.commit('setUserInfo', res.data.data.profile)
          } else {
            store.commit('setUserInfo', {})
          }
          return res
        })
        .catch(error => {
          if (error.response) {
            if (error.response.status === 401) {
              router.push('/login')
            } else {
              store.commit('setUserInfo', {})
            }
          }
        })
    } else {
      const pathname = window.location.pathname
      if (pathname.startsWith('/login')) {
        router.push(pathname.slice(1))
      } else {
        console.log(router)
        router.push('/login')
      }
    }
}

export default { initializeCustomer }