import Vue from 'vue'
import VueRouter from 'vue-router'
import Feed from './components/Feed/Feed'
import FeedSingle from './components/Feed/FeedSingle'
import TopContent from './components/TopContent/TopContent'
import Investment from './components/Investment/Investment'
import Charity from './components/Charity/Charity'
import Home from './components/Home/Home'
import HomeEdit from './components/Home/HomeEdit'
import AddFeedPost from './components/Feed/AddPost'
import AddInvestmentPost from './components/Investment/AddPost'
import AddCharityPost from './components/Charity/AddPost'
import Register from './components/shared/Register'
import Login from './components/shared/Login'
import AddStories from './components/shared/AddStories'
import PersonalContent from './components/shared/PersonalContent'
import Search from './components/shared/Search'
import SelectPhotoVideo from './components/Feed/SelectPhotoVideo'
import Notifications from './components/Notifications/Notifications'
import Rank from './components/Rank/Rank'
import Stories from './components/shared/StoriesView'
import SurfPost from './components/Surf/SurfPost.vue'
import SurfPeople from './components/Surf/SurfPeople.vue'
import PrivacyPolicy from './components/Home/PrivacyPolicy.vue'
import ThermsService from './components/Home/PrivacyPolicy.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  scrollBehavior (to, from, savedPosition) {
    if (['feed', 'top'].indexOf(from.name) !== -1 && to.name === 'personal') {
      return { x: 0, y: 0 }
    }
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
        {
          path: '/',
          component: Home,
          name: 'home',
          meta: { title: 'Home' }
        },
        {
          path: '/stories',
          component: Stories,
          name: 'stories',
          meta: { title: 'Stories' }
        },
        {
          path: '/surf',
          component: SurfPost,
          name: 'surf',
          meta: { title: 'Surf' }
        },
        {
          path: '/surf/people',
          component: SurfPeople,
          name: 'surfPeople',
          meta: { title: 'Surf People' }
        },
        {
          path: '/home/edit',
          component: HomeEdit,
          name: 'HomeEdit',
          meta: {title: 'HomeEdit'}
        },
        {
          path: '/privacy/policy',
          component: PrivacyPolicy,
          name: 'PrivacyPolicy',
          meta: {title: 'Privacy Policy'}
        },
        {
          path: '/therms/service',
          component: ThermsService,
          name: 'ThermsService',
          meta: {title: '/Therms Service'}
        },
        {
          path: '/feed',
          component: Feed,
          name: 'feed',
          meta: { title: 'Feed' }
        },
        {
          path: '/feed/:id',
          component: FeedSingle,
          name: 'feedPost',
          meta: {
            title: 'Post',
            showBackArrow: true
          }
        },
        {
          path: '/top',
          component: TopContent,
          name: 'top',
          meta: { title: 'Top content' }
        },
        {
          path: '/investment',
          component: Investment,
          name: 'investment',
          meta: { title: 'Investment' }
        },
        {
          path: '/charity',
          component: Charity,
          name: 'charity',
          meta: { title: 'Charity' }
        },
        {
          path: '/add/feed',
          component: AddFeedPost,
          name: 'addFeedPost',
          meta: { title: 'Add Feed Post' }
        },
        {
          path: '/add/investment',
          component: AddInvestmentPost,
          name: 'addInvestmentPost',
          meta: { title: 'Add Investment Post' }
        },
        {
          path: '/add/charity',
          component: AddCharityPost,
          name: 'addCharityPost',
          meta: { title: 'Add Charity Post' }
        },
        {
          path: '/add/stories',
          component: AddStories,
          name: 'addStory',
          meta: { title: 'New story' }
        },
        {
          path: '/register',
          component: Register,
          name: 'register',
          meta: { title: 'Register', disableBackAction: true }
        },
        {
          path: '/login/:email?',
          component: Login,
          name: 'login',
          meta: { title: 'Authorization', disableBackAction: true }
        },
        {
          path: '/user/:id',
          component: PersonalContent,
          name: 'personal',
          meta: {
            title: 'Profile',
            showBackArrow: true
          }
        },
      {   path: '/',
          component: SelectPhotoVideo,
          name: 'SelectPhotoVideo',
      },
      {
        path: '/search',
        component: Search,
        name: 'search',
        meta: {
          title: 'Search',
          showBackArrow: true
        }
      },
      {
      path: '/notifications',
      component: Notifications,
      name: 'notifications',
      meta: {
        title: 'Notifications',
        showBackArrow: true
      }
      },
      {
      path: '/rank',
      component: Rank,
      name: 'rank',
      meta: {
        title: 'Rank',
        showBackArrow: true
      }
      }
  ]
})

export default router
