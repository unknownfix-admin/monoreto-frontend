# monoreto

## Recommended using gitflow for distributors

## Build Setup

``` bash
# install dependencies
npm install

# add platfrom
cordova platform add <platform>

# serve with hot reload at localhost:8080
npm run dev

# build application
npm run build <platform>

# run application
npm run <platform>

```
